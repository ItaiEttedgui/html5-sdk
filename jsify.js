let fs = require('fs');
let argv = require('yargs').argv;
let html2js = require('html-to-js');

let fileName = argv['input-file'] || argv['input'] || argv['i'];
let variableName = argv['variable-name'] || argv['variable'] || argv['v'];
let outputFile = argv['output-file'] || argv['output'] || argv['o'];

if (!fileName || !variableName || !outputFile) {
	console.log();
	console.log('please specify all arguments:');
	console.log('--i|input|input-file: input file path');
	console.log('--v|variable|variable-name: javascript variable name which will contain the html string');
	console.log('--o|output|output-file: output file path');

	return;
}

let html = fs.readFileSync(fileName, 'utf-8');
let content = `const ${variableName} = ${html2js(html)};`.replace('module.exports = ', '').replace(/[\r\n]/gm, '');
content += `\nexport default ${variableName};`;

fs.writeFileSync(outputFile, content);