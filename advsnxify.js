const argv = require('yargs').argv;
const esformatter = require('esformatter');
const esformatterRename = require('esformatter-rename');
const fs = require('fs');

const inputFile = argv['input-file'] || argv['input'] || argv['i'];

if (!inputFile) {
	console.log();
	console.log('please specify all arguments:');
	console.log('--i|input|input-file: input file path');

	return;
}

const code = fs.readFileSync(inputFile).toString();

esformatter.register(esformatterRename);
 
// Format our code 
let result = esformatter.format([
  code
].join('\n'), {
  rename: {
    variables: {
      p: 'a',
	  a: 'd',
	  c: 'v',
	  k: 's',
	  e: 'n',
	  d: 'x',
    }
  }
});

fs.writeFile(inputFile, result);