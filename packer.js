let fs = require('fs');
let cheerio = require('cheerio');
let request = require('request');
let argv = require('yargs').argv;


let inputFile = argv['input-file'] || argv['input'] || argv['i'];
let outputFile = argv['output-file'] || argv['output'] || argv['o'];
let overwrite = argv['overwrite'] || argv['v'];

if (!inputFile || (!outputFile && !overwrite)) {
    console.log();
    console.log('please specify all arguments:');
    console.log('--i|input|input-file: input file path');
    console.log('--o|output|output-file: output file path');

    return;
}

if(overwrite){
    outputFile = inputFile;
}

let fileContent = fs.readFileSync(inputFile);

request.post(
    'http://www.danstools.com/javascript-obfuscate/index.php',
    {
        form: {
            ascii_encoding: '62',
            fast_decode: true,
            src: fileContent
        }
    },
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);
            fs.writeFile(outputFile, $('#packed').text());
        }
    }
);