let fs = require('fs');
let argv = require('yargs').argv;
let compile = require('google-closure-compiler-js').compile;

let inputFile = argv['input-file'] || argv['input'] || argv['i'];
let outputFile = argv['output-file'] || argv['output'] || argv['o'];
let overwrite = argv['overwrite'] || argv['v'];

if (!inputFile || (!outputFile && !overwrite)) {
	console.log();
	console.log('please specify all arguments:');
	console.log('--i|input|input-file: input file path');
	console.log('--o|output|output-file: output file path');

	return;
}

if(overwrite){
	outputFile = inputFile;
}

let jsCode = fs.readFileSync(inputFile, 'utf-8');

let flags = {
	compilationLevel: 'SIMPLE',
	env: 'BROWSER',
	jsCode: [{src: jsCode}]
};

let output = compile(flags);
fs.writeFileSync(outputFile, output.compiledCode);