const fs = require('fs');
const argv = require('yargs').argv;

const inputFile = argv['input-file'] || argv['input'] || argv['i'];

if (!inputFile) {
    console.log();
    console.log('please specify all arguments:');
    console.log('--i|input|input-file: input file path');

    return;
}

const amUtils = fs.readFileSync('HTML5SDK/AMUtils.js').toString();
const amsdk = fs.readFileSync('HTML5SDK/amsdk.js').toString();

const version = amUtils.match(/AMTAH5_VERSION:.*\"(.*)\"/)[1];
const title = amsdk.match(/AMTAH5_TITLE.*=.*\"(.*)\"/)[1];

const licenseString = 
`/**
 * @license
 * ${title}
 * Version: ${version}
 * Copyright (c) 5776 (2016) ArtiMedia Technologies LTD
 */`;

const content = fs.readFileSync(argv.input);

fs.writeFile(argv.input, `${licenseString}\n${content}`);
