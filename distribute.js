const fs = require('fs');

const distributions = ['mako'];
const emptyFunction = () => {};

distributions.forEach((d) =>{
	fs.unlink(`C:\\inetpub\\wwwroot\\CDN\\${d}\\html5\\amsdk.js`, emptyFunction);
	fs.unlink(`C:\\inetpub\\wwwroot\\CDN\\${d}\\html5\\${d}.js`, emptyFunction);

	fs.link('C:\\inetpub\\wwwroot\\CDN\\demo.test\\html5\\amsdk.js',`C:\\inetpub\\wwwroot\\CDN\\${d}\\html5\\amsdk.js`, emptyFunction);
	fs.link('C:\\inetpub\\wwwroot\\CDN\\demo.test\\html5\\demo.test.js',`C:\\inetpub\\wwwroot\\CDN\\${d}\\html5\\${d}.js`, emptyFunction);	
});
